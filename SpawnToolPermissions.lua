local spawnToolPermissions = {}

local netplay = require "necro.network.Netplay"
local room = require "necro.client.Room"
local playerList = require "necro.client.PlayerList"
local utils = require "system.utils.Utilities"

spawnToolPermissions.ATTRIBUTE_ID_PERMISSION = netplay.RoomAttribute.extend "Permissions"

function spawnToolPermissions.hasPermission(playerID)
	playerID = playerID or playerList.getLocalPlayerID()
	if playerList.isHost(playerID) then
		return true
	end
	local res = room.getAttribute(spawnToolPermissions.ATTRIBUTE_ID_PERMISSION)
	return type(res) == "table" and res[playerID] or false
end

function spawnToolPermissions.setAllowedPlayers(allowedPlayers)
	return room.setAttribute(spawnToolPermissions.ATTRIBUTE_ID_PERMISSION, utils.fastCopy(allowedPlayers))
end

function spawnToolPermissions.getAllowedPlayers()
	local res = room.getAttribute(spawnToolPermissions.ATTRIBUTE_ID_PERMISSION)
	return type(res) == "table" and utils.fastCopy(res) or {}
end

return spawnToolPermissions

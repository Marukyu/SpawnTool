local spawnToolAction = {}

local action = require "necro.game.system.Action"
local clientActionBuffer = require "necro.client.ClientActionBuffer"
local customActions = require "necro.game.data.CustomActions"
local customEntities = require "necro.game.data.CustomEntities"
local ecs = require "system.game.Entities"
local event = require "necro.event.Event"
local gameInput = require "necro.client.Input"
local player = require "necro.game.character.Player"
local playerList = require "necro.client.PlayerList"
local object = require "necro.game.object.Object"
local objectMap = require "necro.game.object.Map"
local spell = require "necro.game.spell.Spell"
local spawnToolPermissions = require "SpawnTool.SpawnToolPermissions"

spawnToolAction.CONTROL_TYPE = "SpawnTool_TakeControl"

local function handleSpawnAction(args)
	if ecs.typeHasComponent(args.type, "spellcast") then
		local caster
		for _, obj in ipairs(objectMap.getAll(args.x, args.y)) do
			local entity = ecs.getEntityByID(obj)
			if entity:hasComponent("character") then
				caster = entity
				break
			end
		end
		caster = caster or object.spawn("SpawnTool_Spellcaster", args.x, args.y)
		spell.cast(caster, args.type, action.Direction.RIGHT)
	elseif ecs.typeHasComponent(args.type, "position") then
		object.spawn(args.type, args.x, args.y)
	end
end

local function handleControlAction(args, playerID)
	for _, obj in ipairs(objectMap.getAll(args.x, args.y)) do
		local entity = ecs.getEntityByID(obj)
		if entity:hasComponent("controllable") then
			local oldPlayer = entity.controllable.playerID
			local oldControlledEntity = player.getPlayerEntityID(playerID)
			player.setControlledEntity(playerID, entity.id)
			if entity:hasComponent("character") then
				entity.character.canAct = true
			end
			entity.gameObject.active = true
			if entity:hasComponent("visibility") then
				entity.visibility.revealed = true
			end
			if oldPlayer ~= 0 then
				player.setControlledEntity(oldPlayer, oldControlledEntity)
			end
			return
		end
	end
end

spawnToolAction.ID = customActions.registerSystemAction {
	id = "Spawn",
	callback = function (playerID, args)
		if args.type == spawnToolAction.CONTROL_TYPE then
			handleControlAction(args, playerID)
		elseif type(args.type) == "string" and ecs.isValidEntityType(args.type) then
			handleSpawnAction(args)
		end
	end,
}

function spawnToolAction.perform(x, y, entityType, playerID)
	if spawnToolPermissions.hasPermission() then
		gameInput.add(spawnToolAction.ID, playerID, {
			x = x,
			y = y,
			type = entityType,
		})
	end
end

customEntities.register {
	name = "Spellcaster",
	gameObject = {},
	position = {},
	autoDespawn = {},
}

customEntities.register {
	name = "TakeControl",
	gameObject = {},
	position = {},
	autoDespawn = {},
}

return spawnToolAction

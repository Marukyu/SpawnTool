local spawnToolUI = {}

local spawnToolAction = require "SpawnTool.SpawnToolAction"
local spawnToolConfigMenu = require "SpawnTool.SpawnToolConfigMenu"
local spawnToolPermissions = require "SpawnTool.SpawnToolPermissions"

local camera = require "necro.render.Camera"
local chat = require "necro.client.Chat"
local controls = require "necro.config.Controls"
local customActions = require "necro.game.data.CustomActions"
local event = require "necro.event.Event"
local gameState = require "necro.client.GameState"
local hud = require "necro.render.hud.HUD"
local render = require "necro.render.Render"
local ui = require "necro.render.UI"

local color = require "system.utils.Color"
local ecs = require "system.game.Entities"
local gfx = require "system.gfx.GFX"
local transformationMatrix = require "system.gfx.TransformationMatrix"
local input = require "system.game.Input"
local stringUtils = require "system.utils.StringUtilities"
local timer = require "system.utils.Timer"
local utils = require "system.utils.Utilities"

local floor = math.floor
local ceil = math.ceil
local min = math.min
local max = math.max
local clamp = utils.clamp

local isSelectorOpen = false
selectedType = nil
scrollOffset = 0
scrollVelocity = 0

isFiltering = false
local effectiveFilter

cameraOverride = false
cameraOverridePrevMode = nil

showCoords = true

isSelectorOpen = script.persist(function ()
	return isSelectorOpen
end)

local entrySize = 48
local columns = 1

local typeCache = nil

local function getColumnCount()
	return floor(gfx.getWidth() / entrySize)
end

local function invTransformPoint(x, y)
	return transformationMatrix.inverse(render.getTransform(render.Transform.CAMERA)).transformPoint(x, y)
end

local function getTileUnderCursor()
	local x, y = invTransformPoint(input.mouseX(), input.mouseY())
	return floor(x / render.TILE_SIZE + 0.5), floor(y / render.TILE_SIZE + 0.5)
end

local function getIndexUnderCursor()
	local x, y = floor(input.mouseX() / entrySize), floor((input.mouseY() - scrollOffset) / entrySize)
	return x + y * columns + 1
end

local function getEntryPosition(index)
	return ((index-1) % columns) * entrySize, floor((index-1) / columns) * entrySize + scrollOffset
end

local function getOrder(entity)
	return (entity:hasComponent("item") and 0)
		or (entity:hasComponent("enemy") and 1)
		or (entity:hasComponent("playableCharacter") and 2)
		or (entity:hasComponent("spellcast") and 3)
		or 4
end

local function pcallWrap(func)
	return function (...)
		local s, e = pcall(func, ...)
		if not s then
			log.warn("%s", e)
		end
	end
end

local function buildTypeCache()
	local cache = {}

	if not effectiveFilter then
		cache[#cache + 1] = {
			name = "Config",
			order = -1,
			onClick = spawnToolConfigMenu.open,
			texture = "gfx/necro/icons/settings.png",
			width = 12,
			height = 12,
			tx = 0,
			ty = 0,
		}
	end

	local entityTypes = utils.arrayCopy(ecs.getEntityTypesWithComponents {"gameObject", "position"})
	utils.appendToTable(entityTypes, ecs.getEntityTypesWithComponents {"spellcast"})

	utils.removeIf(entityTypes, function (entity)
		return ecs.typeHasComponent(entity, "attachment")
	end)

	if effectiveFilter then
		local pattern = tostring(effectiveFilter):lower()
		pcall(utils.removeIf, entityTypes, function (entity)
			if tostring(entity):lower():match(pattern) then
				return false
			end
			local proto = ecs.getEntityPrototype(entity)
			local name = proto and proto.friendlyName and proto.friendlyName.name
			return not name or not name:lower():match(pattern)
		end)
	end

	for _, typeID in ipairs(entityTypes) do
		local entity = ecs.getEntityPrototype(typeID)
		local sprite
		if entity:hasComponent("sprite") then
			sprite = entity.sprite
		else
			sprite = {
				textureShiftX = 0,
				textureShiftY = 0,
				width = 24,
				height = 24,
			}
		end
		local spriteSheet = entity:hasComponent("spriteSheet") and entity.spriteSheet or {frameX = 1, frameY = 1}
		cache[#cache + 1] = {
			name = typeID,
			texture = sprite.texture,
			width = sprite.width,
			height = sprite.height,
			tx = (spriteSheet.frameX - 1) * sprite.width,
			ty = (spriteSheet.frameY - 1) * sprite.height,
			order = getOrder(entity)
		}
	end

	table.sort(cache, function (a, b)
		if a.order ~= b.order then
			return a.order < b.order
		else
			return a.name < b.name
		end
	end)

	return cache
end

local function setFilter(filter)
	if filter == "" then
		filter = nil
	end
	if effectiveFilter ~= filter then
		effectiveFilter = filter
		typeCache = buildTypeCache()
	end
end

event.tick.add("processSpawnToolUI", {order = "input", sequence = -1}, pcallWrap(function (ev)
	columns = getColumnCount()

	if not spawnToolPermissions.hasPermission() then
		isSelectorOpen = false
		selectedType = nil
		return
	end

	if typeCache == nil then
		typeCache = buildTypeCache()
	end

	if isSelectorOpen then
		scrollVelocity = scrollVelocity + input.scrollY() * 10
		local scrollHeight = ceil(#typeCache / columns) * entrySize - gfx.getHeight()
		scrollOffset = min(0, max(-scrollHeight, scrollOffset + scrollVelocity))
		scrollVelocity = scrollVelocity * 0.75

		if (input.keyDown("lcontrol") or input.keyDown("rcontrol")) and input.keyPress("F") then
			input.suppressKey("F")
			isFiltering = true
			chat.openChatbox("Filter", function ()
				isFiltering = false
			end)
		end
	else
		scrollVelocity = 0
	end

	if isFiltering then
		if not isSelectorOpen or not chat.isChatboxOpen() then
			chat.closeChatbox()
			isFiltering = false
		else
			setFilter(stringUtils.fromUnicode(chat.getChatboxText()))
		end
	end

	if isSelectorOpen and controls.consumeMiscKey(controls.Misc.PAUSE) then
		if isFiltering then
			chat.closeChatbox()
			isFiltering = false
		else
			isSelectorOpen = false
			selectedType = nil
		end
	end
end))

function spawnToolUI.open()
	if not isSelectorOpen then
		isSelectorOpen = true
	else
		return false
	end
end

function spawnToolUI.close()
	if isSelectorOpen then
		selectedType = nil
		isSelectorOpen = false
	else
		return false
	end
end

function spawnToolUI.select()
	if isSelectorOpen then
		isSelectorOpen = false
		selectedType = typeCache[getIndexUnderCursor()]
		if selectedType and selectedType.onClick then
			selectedType.onClick()
			selectedType = nil
		end
	else
		return false
	end
end

function spawnToolUI.spawn()
	if not isSelectorOpen and selectedType then
		local x, y = getTileUnderCursor()
		return spawnToolAction.perform(x, y, selectedType.name)
	else
		return false
	end
end

event.inventoryHUDRenderSlot.add("opacity", {order = "opacity", filter = "itemSlot"}, function (ev)
	if isSelectorOpen then
		ev.opacity = ev.opacity * 0.5
		ev.hideKeybinding = true
	end
end)

event.inventoryHUDRenderSlot.override("itemSlotImage", 1, function (func, ev)
	if isSelectorOpen and ev.slotPosInfo and ev.slotPosInfo.image then
		return render.getBuffer(render.Buffer.UI_HUD).draw {
			rect = hud.getSlotRect(ev.slotPosInfo),
			texture = ev.slotPosInfo.image,
			texRect = ev.slotPosInfo.imageRect,
			z = ev.slotPosInfo.z,
			color = color.opacity(0.5),
		}
	else
		return func(ev)
	end
end)

local drawArgs = {rect = {0, 0, 0, 0}, texRect = {0, 0, 0, 0}, z = -100}

event.renderUI.add("renderUI", {order = "debugOverlay", sequence = -1}, pcallWrap(function (ev)
	if not gameState.isInGame() then
		return
	end

	local bufferUI = render.getBuffer(render.Buffer.UI_CHAT)
	local buffer = render.getBuffer(render.Buffer.UI_DEBUG_OBJECTS)

	local startTime = timer.getGlobalTime()

	if isSelectorOpen and typeCache then
		for i, entry in ipairs(typeCache) do
			if not entry.rendered then
				entry.rendered = true
				if timer.getGlobalTime() - startTime > 0.05 then
					break
				end
			end
			local x, y = getEntryPosition(i)
			if entry.texture then
				drawArgs.texture = entry.texture
				drawArgs.rect[1] = x
				drawArgs.rect[2] = y
				drawArgs.rect[3] = entrySize
				drawArgs.rect[4] = entrySize
				drawArgs.texRect[1] = entry.tx
				drawArgs.texRect[2] = entry.ty
				drawArgs.texRect[3] = entry.width
				drawArgs.texRect[4] = entry.height
				bufferUI.draw(drawArgs)
			elseif entry.name then
				ui.drawText {
					buffer = bufferUI,
					font = ui.Font.SMALL,
					size = ui.Font.SMALL.size,
					x = x + entrySize * (x / (gfx.getWidth() - entrySize)),
					y = y,
					alignX = x / (gfx.getWidth() - entrySize),
					text = entry.name,
					uppercase = false,
					wordWrap = true,
					maxWidth = entrySize,
				}
			end
		end

		local cursor = getIndexUnderCursor()
		local x, y = getEntryPosition(cursor)
		local entry = typeCache[cursor]
		if entry then
			local textInfo = {
				buffer = bufferUI,
				font = ui.Font.SYSTEM,
				x = x + entrySize * (x / (gfx.getWidth() - entrySize)),
				y = y + entrySize,
				alignX = x / (gfx.getWidth() - entrySize),
				alignY = 1,
				text = entry.name,
				z = -2,
			}
			local rect = ui.drawText(textInfo)
			local margin = 2
			bufferUI.draw {
				rect = {rect.x - margin, rect.y - margin, rect.width + margin * 2, rect.height + margin * 2},
				color = color.rgba(0, 0, 0, 200),
				z = -3,
			}
		end

		if chat.isChatboxOpen() then
			local filterY = gfx.getHeight() * (3/4)
			local filterH = 42
			local mouseY = input.mouseY()
			local dist = math.max(filterY - mouseY, mouseY - filterY - filterH)
			bufferUI.draw {
				rect = {0, filterY, gfx.getWidth(), filterH},
				color = color.rgba(0, 0, 0, utils.clamp(80, dist * 5 + 80, 200)),
				z = -1,
			}
		end
	elseif selectedType then
		local entry = selectedType
		local x, y, w, h = render.tileRect(getTileUnderCursor())
		if entry.texture then
			buffer.draw {
				rect = {x, y, w, h},
				texture = entry.texture,
				texRect = {entry.tx, entry.ty, entry.width, entry.height},
			}
		end
		buffer.draw {
			texture = "ext/gui/cursor.png",
			rect = {x, y},
		}
		ui.drawText {
			buffer = buffer,
			font = ui.Font.SMALL,
			x = x,
			y = y,
			alignX = 0,
			alignY = 1,
			text = (showCoords and string.format("%d, %d\n", getTileUnderCursor()) or "") .. entry.name,
			uppercase = false,
		}
	end
end))

event.ecsSchemaReloaded.add("clearCache", "clearCache", function (ev)
	typeCache = nil
end)

camRect = {0, 0, 1, 1}
local camMargin = 100
local camSpeed = 0.1

local camMode = camera.Mode.extend("SpawnToolFreeze", {data = {getTargetRect = function (baseWidth, baseHeight)
	local rect = camRect
	local mx, my = input.mouseX(), input.mouseY()
	rect[1] = rect[1] + camSpeed * (math.min(mx - camMargin, 0) + math.max(mx - gfx.getWidth() + camMargin, 0))
	rect[2] = rect[2] + camSpeed * (math.min(my - camMargin, 0) + math.max(my - gfx.getHeight() + camMargin, 0))
	return rect[1], rect[2], rect[3], rect[4]
end}})

event.render.add("spawnToolCam", {order = "camera", sequence = -1}, function (ev)
	local override = selectedType and input.keyDown("lshift")
	if (not override) ~= (not cameraOverride) then
		cameraOverride = override
		if override then
			cameraOverridePrevMode = camera.getModeOverride()
			camRect[1],camRect[2],camRect[3],camRect[4] = camera.getViewRect()
			camera.setModeOverride(camMode)
		else
			camera.setModeOverride(cameraOverridePrevMode)
		end
	end
end)

function spawnToolUI.isSelectorOpen()
	return isSelectorOpen
end

function spawnToolUI.getSelectedType()
	return selectedType
end

customActions.registerHotkey {
	id = "Open",
	name = "Open spawn menu",
	keyBinding = "Mouse_2",
	callback = spawnToolUI.open,
	enableIf = gameState.isInGame,
}

customActions.registerHotkey {
	id = "Close",
	name = "Close spawn menu",
	keyBinding = "Mouse_2",
	callback = spawnToolUI.close,
	enableIf = gameState.isInGame,
}

customActions.registerHotkey {
	id = "Select",
	name = "Select entity",
	keyBinding = "Mouse_1",
	callback = spawnToolUI.select,
	enableIf = gameState.isInGame,
}

customActions.registerHotkey {
	id = "SpawnEntity",
	name = "Spawn entity",
	keyBinding = "Mouse_1",
	callback = spawnToolUI.spawn,
	enableIf = gameState.isInGame,
}

return spawnToolUI

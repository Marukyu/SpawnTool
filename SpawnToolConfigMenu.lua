local spawnToolConfigMenu = {}

local spawnToolPermissions = require "SpawnTool.SpawnToolPermissions"

local event = require "necro.event.Event"
local menu = require "necro.menu.Menu"
local playerList = require "necro.client.PlayerList"

event.menu.add("config", "SpawnToolConfig", function (ev)
	ev.menu = {}
	ev.menu.label = "Spawn tool"
	ev.menu.entries = {}

	ev.menu.entries[#ev.menu.entries + 1] = {
		label = "Allowed players",
	}

	for _, playerID in ipairs(playerList.getPlayerList()) do
		ev.menu.entries[#ev.menu.entries + 1] = {
			label = function ()
				return string.format("%s: %s", playerList.getName(playerID), spawnToolPermissions.hasPermission(playerID))
			end,
			action = function ()
				local players = spawnToolPermissions.getAllowedPlayers()
				players[playerID] = not players[playerID]
				spawnToolPermissions.setAllowedPlayers(players)
			end,
		}
	end

	ev.menu.entries[#ev.menu.entries + 1] = {}
	ev.menu.entries[#ev.menu.entries + 1] = {
		label = "OK",
		action = menu.close,
	}
end)

function spawnToolConfigMenu.open()
	if playerList.isHost() then
		menu.open("SpawnToolConfig")
	end
end

return spawnToolConfigMenu
